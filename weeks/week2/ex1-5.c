#include <stdio.h>
#include <limits.h>
#include <float.h>
#include <string.h>

void ex1(){
    printf("\nexercise 1\n");
    int i = INT_MAX;
    float f = FLT_MAX;
    double d = DBL_MAX;
    printf("sizes of: \nint %lu\nfloat %lu\ndouble %lu\n",sizeof(int),sizeof(float),sizeof(double));
    printf("max int %d\nmax float %f\nmax double %f\n",i,f,d);
}

void ex2(){
    printf("\nexercise 2\n");
    printf("input string:\n");
    char s[100];
    fgets(s,LINE_MAX,stdin);
    unsigned int s_len = strlen(s);
    for(int u =(int) s_len-1;u>=0;--u){
        putc(s[u],stdout);
    }
    printf("\n");
}

void ex3(){
    printf("\nexercise 3\n");
    printf("input height:\n");
    int n ;
    char ch[100];
    fgets(ch,LINE_MAX,stdin);
    sscanf(ch, "%d",&n);
    if (n < 0) return;
    int l = 2*n-1;
    char out[l+1];
    for (int h = 0;h<l;h++){
        out[h] =' ';
    }
    out[l] = '\0';
    for (int g = 0;g!=n;g++){
        out[l/2 + g] = '*';
        out[l/2 - g] = '*';
        printf("%s\n",out);
    }
}

void swap(int *a,int *b){
    int t = *a;
    *a = *b;
    *b = t;
}

void ex4(){
    printf("\nexercise 4\n");
    printf("input two numbers (a,b) with space separator:\n");
    int a,b;
    fscanf(stdin,"%d %d",&a,&b);
    swap(&a,&b);
    printf("swapped: a = %d, b = %d",a,b);

}

void ex5(){
    printf("\nexercise 5\n");
    printf("input height and type (0-2) with space separation:\n");
    int n ;
    int t ;
    fscanf(stdin,"%d %d",&n,&t);
    if (n < 0) return;
    if (t>2 || t<0)return;
    int l = n;
    char out[l+1];
    for (int h = 0;h<l;h++){
        if (t == 2){
            out[h] ='*';
            continue;
        }
        out[h] =' ';
    }
    out[l] = '\0';
    for (int g = 0;g!=n;g++){
        if (t==1 && ((n%2 != 0 && g > n/2 ) || (n%2 == 0 && g >= n/2 ))){
            out[n-g] = ' ';
        }else if (t != 2){
            out[g] = '*';
        }
        printf("%s\n",out);
    }

}
int main() {
    ex1();
    ex2();
    ex3();
    ex4();
    ex5();
    return 0;
}
