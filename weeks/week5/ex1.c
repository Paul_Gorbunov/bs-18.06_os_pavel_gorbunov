#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>


#define K  5

int G = 0;
pthread_mutex_t lock;

void *thread_f(void *varg){
    pthread_mutex_lock(&lock);
    printf("Thread number %d is running.\n",*((int*)varg));
    printf("Thread number %d exited.\n",*((int*)varg));
    pthread_mutex_unlock(&lock); 
}

int main(){
    int S = 0;
    int t_c = 3;
    pthread_t ids[t_c];
    if (pthread_mutex_init(&lock,NULL) != 0){
        printf("bad\n");
        return 0;
    }
    for (int h = 0;h<t_c;h++){
        pthread_t id;
        ids[h] = id;
        void *pass = malloc(sizeof(int));
        *((int*)pass) = h;
        pthread_create((ids+h),NULL,thread_f,pass);
        printf("Created thread %d.\n",h);
       
    }
    for (int h = 0;h<t_c;h++){
     pthread_join(ids[h],NULL);
    }
    pthread_mutex_destroy(&lock);

    return 0;
}
