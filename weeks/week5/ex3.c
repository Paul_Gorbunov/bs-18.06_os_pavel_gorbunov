#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define L 1
#define S 10000
#define P 1000

int full = 0;
int buffer[L];

int TIME = 0;


void *thread_f(void *varg){
    if (!*((int*)varg)){
        printf("I am producer\n");
        while (TIME < S){
            if (! full || 1){ 
                for (int i = 0;i<L;i++){
                    buffer[i] += 1;
                }
                full = 1;
            }
            TIME = TIME + 1;
            if (TIME % P == 0 || TIME % P <= 10) printf("%d\n",buffer[0]);
        }
        
    }else{
        printf("I am consumer\n");
        while(TIME < S){
            if (full || 1){
                for (int i = 0;i<L;i++){
                    buffer[i] -=1;
                }
                full = 0;
            }
            TIME = TIME + 1;
        }
    }
}

int main(){
    int t_c = 2;
    pthread_t ids[t_c];

    for (int h = 0;h<t_c;h++){
        pthread_t id;
        ids[h] = id;
        void *pass = malloc(sizeof(int));
        *((int*)pass) = h;
        pthread_create((ids+h),NULL,thread_f,pass);
       
    }
    for (int h = 0;h<t_c;h++){
     pthread_join(ids[h],NULL);
    }

    return 0;
}
