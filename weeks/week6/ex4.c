#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>


int run = 1;

void sign_handler(int i){
    printf("\nexiting becouse of %d signal\n",i);
    run = 0;
}

int main(){
    
    signal(SIGINT, sign_handler);
    while(run){
        printf("running\n");
        sleep(7);
    }

    return 0;
}
