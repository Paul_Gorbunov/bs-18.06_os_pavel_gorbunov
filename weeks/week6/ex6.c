#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>

int run[1] = {1};

void sig_handler(int i){
    if (i == 15){
        printf("process 2: exiting becouse of %d signal\n",i);
        run[0] = 0;
    }
}

void task1(void *args){
    printf("process 1: start\n");
    int r_p = *(int *)args;
    int res[2] = {1,0};
    int fl = 1;
    while(res[0]){
        read(r_p,res,sizeof(res));
    }
    printf("process 1: received the pid#2 and waiting 5 sec\n");
    sleep(5);
    printf("process 1: stopping proc#2 (%d)\n",res[1]);
    kill(res[1],SIGSTOP);
    printf("process 1: exiting\n");
    close(r_p);
}

void task2(void *args){
    printf("process 2: start\n");
    int r_p = *(int *)args;
    signal(SIGTERM,sig_handler);
    while (run[0]){
        printf("process 2: is running\n");
        sleep(2);
    }
}

int process(void (*funct)(void *),void *args){
    int pid = fork();
    if (pid == 0){
        (*funct)(args);
        exit(0);
    }
    return pid;
}

int main(){
    printf("main proc: start\n");
    int p[2][2] = {{1,2},{3,4}};
    for (int u = 0;u<2;u++){
        pipe(p[u]);
    }
    printf("main proc: starting proc#1 and proc#2\n");
    int pid1 = process(task1,(void*)&(p[0][0]));
    int pid2 = process(task2,(void*)&(p[1][0]));
    sleep(2);
    printf("main proc: sending pid#2 to the proc#1\n");
    int s[2] = {0,pid2};
    write(p[0][1],s,sizeof(s));
    printf("main proc: waitpid for proc#2 (%d)\n",pid2);
    int stat;
    waitpid(pid2,&stat,WUNTRACED);
    printf("main proc: sending SIGNCONT to proc#2 and sleep for 3 sec\n");
    kill(pid2,SIGCONT);
    sleep(3);
    printf("main proc: terminating proc#2 and exiting\n");
    kill(pid2,SIGTERM);
    close(p[0][0]);
    return 0;
}
