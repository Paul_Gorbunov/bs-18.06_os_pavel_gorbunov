#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 

#define SIZE 5

int main(){
    int p[2];
    if (pipe(p)==-1){ 
        printf("error1\n");
        exit(1); 
    } 
    
    if (fork() > 0){
        char* str = "hello";
        write(p[1],str,SIZE);
        printf("sent: %s\n",str);
        close(p[1]);
        
    }else{ 
        sleep(0.1);
        char str2[SIZE+1];
        read(p[0],str2,SIZE+1);
        printf("recived: %s\n",str2);
        close(p[0]);
        
    }
    
    return 0;
}
