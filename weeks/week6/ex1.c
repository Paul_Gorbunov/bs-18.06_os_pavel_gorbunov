#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 


#define SIZE 8

int main(){
    int p[2];
    if (pipe(p)==-1){ 
        exit(1); 
    } 
    char str1[SIZE] = "       \0";
    char str2[SIZE] = "Message\0";
    
    printf("1 '%s'   2 '%s'\n",str1, str2);
    write(p[1],str2,SIZE);    
    read(p[0],str1,SIZE);
    printf("1 '%s'   2 '%s'\n",str1, str2);
    close(p[0]);
    close(p[1]);
    return 0;
}

