#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

int run[1] = {1};

void sig_handler(int i){
    int id = 0;
    if (i == 15){
        printf("\nexiting becouse of %d signal (%d)\n",i,id);
        run[id] = 0;
    }
}

int main(){
    int pid = fork();
    if (pid == 0){
        int id = 0;
        signal(SIGTERM,sig_handler);
        while(run[id]){
            printf("alive 1\n");
            sleep(1);
        }
    }else{
        sleep(10);
        kill(pid,SIGTERM);
    }
    
    
    return 0;
}
