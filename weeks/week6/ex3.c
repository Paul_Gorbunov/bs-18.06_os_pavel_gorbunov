#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h> 

int run = 1;

void sign_handler(int i){
    if (i == 2){
        printf("\nexiting becouse of %d signal\n",i);
        run = 0;
    }
    
}

int main(){
    
    signal(SIGINT, sign_handler);
    while(run){
        printf("running\n");
        sleep(2);
    }

    return 0;
}
